<?php 
    if (!isset($user)){
        $user->role = isset($user->role) && !empty($user->role);
    }
?>
<article>
    <?php if($user->role == 'admin'):?>
        <?=$this->html->link('Edit', '/a/edit/'.$article->slug, array(
            'class' => 'btn pull-right'
        )); ?>
    <?php endif; ?>
    <h1><?=$article->title ?></h1>
    <hr/>
    <p><?=$article->body ?></p>
    <hr/>
    <p><?=$this->TagHelper->tagsToInlineLabels( $article->tags ) ?></p>
</article>