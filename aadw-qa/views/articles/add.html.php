<?php 
$this->form->config(
    array( 
        'templates' => array( 
            'error' => '<div class="alert alert-error">{:content}</div>' 
        )
    )
); 
?>
<?=$this->form->create($article); ?>
    <?=$this->form->field('title');?>
    <?=$this->form->field('body',array(
        'type' => 'textarea',
        'cols' => '100',
        'rows' => '10'
    )); ?>
    <?=$this->form->field( 
		'tags', 
		array( 
			'type' => 'array', 
			'value' => $this->TagHelper->tagsToString( $article->tags )
		) 
	);?>
    <?=$this->form->submit('Add Article', array(
        'class' => 'btn'
    )); ?>
<?=$this->form->end(); ?>