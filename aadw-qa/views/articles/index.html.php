<h1>DIY Articles</h1>
<hr/>
<?php foreach($articles as $article): ?>
<article>
    <h2><?=$this->html->link($article->title, '/a/'.$article->slug); ?></h2>
</article>
<hr/>
<?php endforeach; ?>
<?=$this->BootstrapPaginator->paginate(); ?>