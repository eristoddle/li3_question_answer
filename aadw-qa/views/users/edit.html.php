<div id="ribbon">
	<span>Profile Modifications</span>
</div>
<div id="content-wrapper">
	<div style="width: 500px; margin-left: auto; margin-right: auto;">
		<?=$this->form->create($user);?>
            <?php 
                $this->form->config(
                    array( 
                        'templates' => array( 
                            'error' => '<div class="alert alert-error">{:content}</div>' 
                        )
                    )
                ); 
            ?>
			<?=$this->form->field('name');?>
			<?=$this->form->submit('Update', array(
                'class' => 'btn'
            )); ?>
		<?=$this->form->end();?>
	</div>
</div>