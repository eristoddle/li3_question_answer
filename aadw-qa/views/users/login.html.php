<div id="ribbon">
	<span>Login</span>
</div>
<div id="content-wrapper">
	<div>
		<?=$this->form->create();?>
            <?php 
                $this->form->config(
                    array( 
                        'templates' => array( 
                            'error' => '<div class="alert alert-error">{:content}</div>' 
                        )
                    )
                ); 
            ?>
			<?=$this->form->field('email');?>
			<?=$this->form->field('password',array('type'=>'password'));?>
			<?=$this->form->submit('Login', array(
                'class' => 'btn'
            )); ?>
		<?=$this->form->end();?>
	</div>
</div>