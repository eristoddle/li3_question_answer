<div id="ribbon">
	<span>Dashboard</span>
</div>
<div id="content-wrapper">
	<ul id="panel">
		<h3>Menu</h3>
		<li><?=$this->html->link('Logout', array('Users::logout'));?></li>
	<?php if($user->role == 'admin'):?>
        <h3>Admin</h3>
		<li><?=$this->html->link('Users', array('Users::admin_index', 'admin' => true));?></li>
        <li><?=$this->html->link('Articles', array('Articles::add', 'admin' => true));?></li>
        <li><?=$this->html->link('Videos', array('Videos::add', 'admin' => true));?></li>
        <li><?=$this->html->link('Questions', array('Posts::index', 'admin' => true));?></li>
	<?php endif;?>
	</ul>
	<div>
		<h2>Welcome <?=$user->name;?></h2>
	</div>
	<br />
</div>