<h1><?=$tag->tag ?></h1>
<hr/>
<?php foreach($tag->content as $content): ?>
    <article>
        <?php $view = str_replace('app\\models\\', '', $content->model)."::View"; ?>
        <h2><?=$this->html->link($content->title,array(
            $view,
            'slug' => $content->slug)); ?></h2>
        <p><?=str_replace('app\\models\\', '', $content->model); ?></p>
    </article>
    <hr/>
<?php endforeach; ?>