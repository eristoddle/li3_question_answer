<?php if(!isset($post)) {$post = null; }?>
<?php if(!isset($title)) {$title = null; }?>

<?=$this->form->create($post, array(
            'action' => 'add'
        )); ?>
<?php 
$this->form->config(
    array( 
        'templates' => array( 
            'error' => '<div class="alert alert-error">{:content}</div>' 
        )
    )
); 
?>
    <? if($title): ?>
        <?=$this->form->field('title',array(
            'value' => $title
        )); ?>
    <? endif; ?>
    <? if(!$title): ?>
        <?=$this->form->field('title'); ?>
    <? endif; ?>
    <?=$this->form->field('description',array(
        'type' => 'textarea',
        'cols' => '100',
        'rows' => '10'
    )); ?>
    <?=$this->form->field('tags');?>
    <?=$this->form->field('post_type',array(
        'type' => 'hidden',
        'value' => 'question'
    )); ?>
    <?=$this->form->submit('Ask'); ?>
<?=$this->form->end(); ?>

<h3>Upload a Picture</h3>
    
<?=$this->form->create(null,array('type' => 'file')); ?>
    <?=$this->form->field('pic_1', array(
        'type' => 'file',
        'label' => 'Upload Picture'
    )); ?>
    <?=$this->form->field('post_id',array(
        'type' => 'hidden',
        'value' => $post->_id
    )); ?>
    <?=$this->form->submit('Save'); ?>
<?=$this->form->end(); ?>
<?php
	/*Data Dump*/
//	echo '<pre>';
//	print_r($post);
//    print_r($title);
//	echo '</pre>';
?>