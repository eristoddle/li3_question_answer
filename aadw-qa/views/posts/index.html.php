<?php
    if (!isset($post)){
        $post = null;
    }
?>

<?=$this->form->create($post, array(
            'action' => 'prefill'
        )); ?>
    <?php 
    $this->form->config(
        array( 
            'templates' => array( 
                'error' => '<div class="alert alert-error">{:content}</div>' 
            )
        )
    ); 
    ?>
    <?=$this->form->field('title', array(
            'label' => 'Question'
        )); ?>
    <?=$this->form->field('post_type',array(
        'type' => 'hidden',
        'value' => 'question'
    )); ?>
    <?=$this->form->submit('Ask', array(
            'class' => 'btn'
        )); ?>
<?=$this->form->end(); ?>

<?php if (count($posts)): ?>
    <? foreach($posts as $post): ?>
        <article>
            <h2>
                <? //TODO: No hard paths ?>
                <?=$this->html->link($post->title,'/q/'.$post->slug)?>
                </a>
            </h2>
            <p><?=$post->description; ?></p>
        </article>
    <hr/>
    <? endforeach; ?>
<?php endif; ?>
<?=$this->BootstrapPaginator->paginate(); ?>
