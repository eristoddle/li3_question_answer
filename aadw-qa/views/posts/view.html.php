<article>
    <h1>
        <?=$post->title; ?>
    </h1>
    <?=$this->html->link('Edit', "/q/edit/".$post->slug, array(
        'class' => 'btn pull-right'
    )); ?>
    <p><?=$post->description; ?></p>
    <p><?=$this->TagHelper->tagsToInlineLabels( $post->tags ) ?></p>
</article>

<?php if(isset($pics)): ?>
    <hr />
    <ul class="thumbnails">
        <?php foreach($pics as $pic): ?>
            <li class="thumbnail span3">
                <a data-toggle="modal" href="#<?=$pic->_id; ?>" >
                    <?php $pic_array = explode(".",$pic->filename); ?>
                    <?=$this->html->image("/pics/view/{$pic->_id}.".$pic_array[1]); ?>
                </a>
                <div class="modal fade" id="<?=$pic->_id; ?>" style="display:none;">
                    <?=$this->html->image("/pics/view/{$pic->_id}.".$pic_array[1]); ?>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
    <hr />
<?php endif; ?>

<?php if (count($answers)): ?>
<h2>Answers</h2>
    <? foreach($answers as $answer): ?>
        <div class="well">
            <h3>
                <?=$answer->title; ?>
            </h3>
            <p><?=$answer->description; ?></p>
        </div>
    <? endforeach; ?>
<hr />
<?php endif; ?>
<h2>Answer Question</h2>
<?php 
$this->form->config(
    array( 
        'templates' => array( 
            'error' => '<div class="alert alert-error">{:content}</div>' 
        )
    )
); 
?>
<?php 
$this->form->config(
    array( 
        'templates' => array( 
            'error' => '<div class="alert alert-error">{:content}</div>' 
        )
    )
); 
?>
<?=$this->form->create(null,array(
            'action' => 'add'
)); ?>
    <?=$this->form->field('title',array(
            'label' => 'Answer'
        )); ?>
    <?=$this->form->field('description',array(
        'type' => 'textarea',
        'cols' => '100',
        'rows' => '10'
    )); ?>
    <?=$this->form->field('post_type',array(
        'type' => 'hidden',
        'value' => 'answer'
    )); ?>
    <?=$this->form->field('question_slug',array(
        'type' => 'hidden',
        'value' => $post->slug
    )); ?>
    <?=$this->form->submit('Answer', array(
            'class' => 'btn'
        )); ?>
<?=$this->form->end(); ?>