<?php if(!isset($post)) {$post = null; }?>
<?=$this->form->create($post); ?>
<?php 
$this->form->config(
    array( 
        'templates' => array( 
            'error' => '<div class="alert alert-error">{:content}</div>' 
        )
    )
); 
?>
    <?=$this->form->field('title'); ?>
    <?=$this->form->field('description',array(
        'type' => 'textarea',
        'cols' => '100',
        'rows' => '10'
    )); ?>
    <?=$this->form->field( 
		'tags', 
		array( 
			'type' => 'array', 
			'value' => $this->TagHelper->tagsToString( $post->tags )
		) 
	);?>
    <?=$this->form->field('post_type',array(
        'type' => 'hidden',
        'value' => 'question'
    )); ?>
    <?=$this->form->field('slug',array(
        'type' => 'hidden',
    )); ?>
    <div class="btn-group">
            <?=$this->form->submit('Edit', array(
                'class' => 'btn'
            )); ?>
        <?=$this->html->link('Done!', "/q/".$post->slug, array(
            'class' => 'btn'
        )); ?>
    </div>
    
<?=$this->form->end(); ?>

<?php if(isset($pics)): ?>
    <hr />
    <ul class="thumbnails">
        <?php foreach($pics as $pic): ?>
            <li class="thumbnail span2">
                <?php $pic_array = explode(".",$pic->filename); ?>
                <?=$this->html->image("/pics/view/{$pic->_id}.".$pic_array[1]); ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<h3>Add a Picture</h3>
    
<?=$this->form->create(null,array(
    'type' => 'file',
    'url' => '/pics/add/'
    )); ?>
    <?=$this->form->field('file', array(
        'type' => 'file',
        'label' => 'Upload Picture'
    )); ?>
    <?=$this->form->field('post_id',array(
        'type' => 'hidden',
        'value' => $post->_id
    )); ?>
    <?=$this->form->field('post_slug',array(
        'type' => 'hidden',
        'value' => $post->slug
    )); ?>
    <?=$this->form->submit('Upload', array(
            'class' => 'btn'
        )); ?>
<?=$this->form->end(); ?>