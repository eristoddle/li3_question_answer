<ul class="thumbnails">
<?php foreach ($pics as $pic): ?>
    <li class="span3">
        <div class="thumbnail">
            <?=$this->html->image("/pics/view/{$pic->_id}.jpg"); ?>
            <div class="caption">
                <h5><?=$this->html->link(
                    $pic->title,
                    array('pics::view', 'id' => $pic->_id)
                ); ?></h5>
            </div>
        </div>
    </li>
<?php endforeach ?>
</ul>
