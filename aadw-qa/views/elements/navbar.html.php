<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <?=$this->html->link('DIY site', 'Posts::index', array('class' => 'brand')); ?>
            <ul class="nav">
                <li><?=$this->html->link('Questions & Answers', 'Posts::index'); ?></li>
                <li><?=$this->html->link('Articles', 'Articles::index'); ?></li>
                <li><?=$this->html->link('Videos', 'Videos::index'); ?></li>
            </ul>
            <ul class="nav pull-right">
                <li><?=$this->html->link('Login', '/login'); ?></a></li>
                <li><?=$this->html->link('Logout', '/logout'); ?></a></li>
                <li><?=$this->html->link('Dash', '/dashboard'); ?></a></li>
                <li><?=$this->html->link('Profile', '/settings'); ?></li>
                <li><?=$this->html->link('Register', '/register'); ?></li>
            </ul>
        </div>
    </div>
</div>
