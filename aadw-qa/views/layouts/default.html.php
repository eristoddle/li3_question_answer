<!doctype html>
<html>
<head>
	<?php echo $this->html->charset();?>
	<title>Application &gt; <?php echo $this->title(); ?></title>
	<?php echo $this->html->style(array('debug', 'lithium')); ?>
    <?php echo $this->html->style(array('bootstrap','bootstrap-responsive','app')); ?>
	<?php echo $this->scripts(); ?>
    <?= $this->styles(); ?>
    <!--Loading jQuery twice until I figure this head js out. says $ doesn't exist if I don't -->
    <?=$this->html->script(array('head.js','jquery.min'));?>
	<?php echo $this->html->link('Icon', null, array('type' => 'icon')); ?>
</head>
<body class="app">
    <?=$this->_render('element', 'navbar');?>
    <?=$this->_render('element', 'header');?>
	<div id="container">
		<div id="content">
            <?=$this->flashMessage->output(); ?>
			<?php echo $this->content(); ?>
		</div>
	</div>
    <script type="text/javascript" charset="utf-8">
		head.js(
			"<?=$this->url('/js/jquery.min.js');?>",
			"<?=$this->url('/js/icanhaz.min.js');?>",
			"<?=$this->url('/js/bootstrap.min.js');?>",
			"<?=$this->url('/js/app.js');?>",
			function() {
				ich.grabTemplates();
			}
		);
	</script>
</body>
</html>