<h1>DIY Videos</h1>
<hr/>
<?php foreach($videos as $video): ?>
<article>
    <h2><?=$this->html->link($video->title, '/v/'.$video->slug); ?></h2>
</article>
<hr/>
<?php endforeach; ?>
<?=$this->BootstrapPaginator->paginate(); ?>