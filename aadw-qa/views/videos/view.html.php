<?php 
    if (!isset($user)){
        $user->role = isset($user->role) && !empty($user->role);
    }
?>
<article>
    <?php if($user->role == 'admin'):?>
        <?=$this->html->link('Edit', '/v/edit/'.$video->slug, array(
        'class' => 'btn pull-right'
    )); ?>
    <?php endif; ?>
    <h1><?=$video->title ?></h1>
    <iframe width="640" height="385" src="<?=$video->youtube_embed_url ?>" frameborder="0" allowfullscreen></iframe>
    <p><?=$video->body ?></p>
    <p><?=$this->TagHelper->tagsToInlineLabels( $video->tags ) ?></p>
</article>