<?php 
$this->form->config(
    array( 
        'templates' => array( 
            'error' => '<div class="alert alert-error">{:content}</div>' 
        )
    )
); 
?>
<?=$this->form->create($video); ?>
    <p>Use the share URL that starts with http://youtu.be</p>
    <?=$this->form->field('youtube_url');?>
    <?=$this->form->field('title');?>
    <?=$this->form->field('body',array(
        'type' => 'textarea',
        'cols' => '100',
        'rows' => '10'
    )); ?>
    <?=$this->form->field( 
		'tags', 
		array( 
			'type' => 'array', 
			'value' => $this->TagHelper->tagsToString( $video->tags ), 
		) 
	);?>
    <?=$this->form->submit('Save Video', array(
        'class' => 'btn'
    )); ?>
<?=$this->form->end(); ?>