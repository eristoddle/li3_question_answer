<?php

namespace app\controllers;

use app\models\Posts;
use lithium\action\DispatchException;
use app\models\Pics;

class PostsController extends \lithium\action\Controller {
    
    public $publicActions = array('index','view');

	public function index() {
        $limit = 10;
        $page = $this->request->page ?: 1;
        $skip = ($page - 1)*10;
        $post_type = array('post_type' => 'question');
        $total = Posts::count(array(
            'conditions' => array(
            'post_type' => 'question')));
        $posts = Posts::find('all', array(
            'conditions' => array(
                'post_type' => 'question'
            ),
            'limit' => $limit,
            'offset' => $skip,
            'order' => array('modified' => 'DESC')
        ));
        return compact('posts', 'total', 'page', 'limit');
	}
    
	public function view() {
		$post = Posts::find('first', array(
				'conditions'=> array(
                    'slug' => $this->request->slug
                )
			)
		);
        
        $pics = Pics::find('all', array(
            'conditions' => array(
            'post_slug' => $this->request->slug
            )
        ));
        
        $answers = Posts::find('all', array(
            'conditions' => array(
            'post_type' => 'answer',
            'question_slug' => $this->request->slug
            )
        ));
		return compact('post', 'pics', 'answers');
	}

	public function add() {
		$post = Posts::create();
		
        //TODO: Move this switch to filter in model
		if ($this->request->data && $post->save($this->request->data)) {
            if($post->question_slug){
                return $this->redirect("/q/".$post->question_slug);
            }else{
                return $this->redirect("/q/".$post->slug);
            }
		}
		return compact('post');
	}
    
    public function prefill() {       
        $post = Posts::create();
        
        if ($this->request->data && $post->save($this->request->data)) {
            return $this->redirect("/q/edit/".$post->slug);
        }
        
        $this->_render['template'] = 'edit';

		return compact('post');
	}

	public function edit() {
		$post = Posts::find('first', array(
				'conditions'=> array(
                    'slug' => $this->request->slug
                )
			)
		);
        
        $pics = Pics::find('all', array(
            'conditions' => array(
            'post_slug' => $this->request->slug
            )
        ));

		if (!$post) {
			return $this->redirect('Posts::index');
		}
		if (($this->request->data) && $post->save($this->request->data)) {
			return $this->redirect("/q/edit/".$post->slug);
		}
		return compact('post', 'pics');
	}

	public function delete() {
		if (!$this->request->is('post') && !$this->request->is('delete')) {
			$msg = "Posts::delete can only be called with http:post or http:delete.";
			throw new DispatchException($msg);
		}
		Posts::find($this->request->id)->delete();
		return $this->redirect('Posts::index');
	}
}

?>