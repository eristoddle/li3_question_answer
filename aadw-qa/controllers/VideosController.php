<?php

namespace app\controllers;

use app\models\Videos;
use app\models\Users;
use lithium\storage\Session;
use lithium\action\DispatchException;
use li3_flash_message\extensions\storage\FlashMessage;

class VideosController extends \lithium\action\Controller {
    
    public $publicActions = array('index','view');

	public function index() {
		$limit = 10;
        $page = $this->request->page ?: 1;
        $order = array('created' => 'DESC');
        $total = Videos::count();
        $videos = Videos::all(compact('order','limit','page'));
        return compact('videos', 'total', 'page', 'limit');
	}

	public function view() {
        $video = Videos::find('first', array(
				'conditions'=> array(
                    'slug' => $this->request->slug
                )
			)
		);
		
        $user = Users::find(Session::read('user._id'));
        
		return compact('video', 'user');
	}

	public function add() {
        $user = Users::find(Session::read('user._id'));
        
        if($user->role == 'admin') {
            $video = Videos::create();

            if (($this->request->data) && $video->save($this->request->data)) {
                return $this->redirect(array('Videos::view', 'slug' => $video->slug));
            }
            return compact('video');
        }
        
        FlashMessage::write('Only admin users can access this page.');
        return $this->redirect('Videos::index');
	}

	public function edit() {
        $user = Users::find(Session::read('user._id'));
        
        if($user->role == 'admin') {
            $video = Videos::find('first', array(
                    'conditions'=> array(
                        'slug' => $this->request->slug
                    )
                )
            );

            if (!$video) {
                return $this->redirect('Videos::index');
            }
            if (($this->request->data) && $video->save($this->request->data)) {
                return $this->redirect(array('Videos::view', 'slug' => $video->slug));
            }
            return compact('video');
        }
        
        FlashMessage::write('Only admin users can access this page.');
        return $this->redirect('Videos::index');
	}

	public function delete() {
		if (!$this->request->is('post') && !$this->request->is('delete')) {
			$msg = "Videos::delete can only be called with http:post or http:delete.";
			throw new DispatchException($msg);
		}
		Videos::find($this->request->id)->delete();
		return $this->redirect('Videos::index');
	}
}

?>