<?php

namespace app\controllers;

use app\models\Users;
use lithium\action\DispatchException;
use li3_flash_message\extensions\storage\FlashMessage;
use lithium\security\Auth;
use lithium\storage\Session;
use lithium\analysis\Logger;

class UsersController extends \lithium\action\Controller{
    
    public $publicActions = array('login','logout','add');
    
	public function dashboard(){
		if(!Auth::check('user')){
			FlashMessage::write('Only logged-in users can access this page.');
			return $this->redirect('Users::login');
		}

		$user = Users::find(Session::read('user._id'));
        
		return compact('user');
	}
    
	public function admin_index(){
		$limit = 20;
		$page = $this->request->page ?: 1;
		$order = array('name' => 'ASC');
		$total = Users::count();
        
		$users = Users::all(compact('order','limit','page'));

		return compact('users', 'total', 'page', 'limit');
	}

	public function admin_add(){
		$user = Users::create();
        
		if (!empty($this->request->data)){
			try{
				$saved = $user->save($this->request->data);
			}catch(\Exception $e){
				FlashMessage::write($e->getMessage());
				return compact('user');
			}
			if ($saved){
				return $this->redirect(array("Users::index", 'admin'=>true));
			}
		}
        
		return compact('user');
	}

	public function admin_edit(){
		$conditions = array(
			'_id' => $this->request->id
		);
		$user = Users::first(compact('conditions'));

		if (!$user){
			FlashMessage::write('User not found.');
			return $this->redirect($this->request->referer());
		}

		if (!empty($this->request->data)){
			try{
				$saved = $user->save($this->request->data);
			}catch(\Exception $e){
				FlashMessage::write("The user could not be modified.");
				return compact('user');
			}
			if ($saved){
				FlashMessage::write("User modified.");
				return $this->redirect(array("Users::index", 'admin'=>true));
			}else{
				FlashMessage::write("The user could not be modified.");
			}
		}
        
		return compact('user');
	}

	public function admin_enable(){
		$data = array('success'=>false,'active'=>false, 'id'=>$this->request->id);

		if (empty($this->request->id)){
			return $this->render(array('json'=>$data));
		}

		$conditions = array('_id'=>$this->request->id);
		$user = Users::first(compact('conditions'));

		if (!$user){
			return $this->render(array('json' => $data));
		}

		if ($user->setActive(true)){
			$data['active'] = true;
			$data['success'] = true;
		}
        
		return $this->render(array('json' => $data));
	}

	public function admin_disable(){
		$data = array('success'=>false,'active'=>false, 'id'=>$this->request->id);

		if (empty($this->request->id)){
			return $this->render(array('json'=>$data));
		}

		$conditions = array('_id'=>$this->request->id);
		$user = Users::first(compact('conditions'));

		if (!$user){
			return $this->render(array('json' => $data));
		}

		if ($user->setActive(false)){
			$data['active'] = false;
			$data['success'] = true;
		}

		return $this->render(array('json' => $data));
	}

	public function reset_password(){}

	public function edit(){
		$conditions = array('_id' => Session::read('user._id'));
        
		$user = Users::first(compact('conditions'));
        
		if (!empty($this->request->data)){
			$user->set($this->request->data);
			if ($user->save($this->request->data,array('whitelist'=>array('name')))){
				FlashMessage::write("Profile updated.");
				return $this->redirect('/');
			}
		}
        
		return compact('user');
	}
	public function login(){
		if (!empty($this->request->data)){
			if (Auth::check('user', $this->request)){
				return $this->redirect('Users::dashboard');
			}else{
				FlashMessage::write("Wrong email or password.");
			}
		}
	}

	public function logout(){
        Auth::clear('user');
        FlashMessage::write("Your session has been terminated.");
        return $this->redirect('/');
	}

	public function add(){
		$user = Users::create();
        
		if (!empty($this->request->data)){
			try{
				$saved = $user->save($this->request->data);
			}catch(\Exception $e){
				FlashMessage::write($e->getMessage());
				return compact('user');
			}

			if ($saved){
				Auth::check('user', $this->request);
				return $this->redirect('/');
			}
		}
        
		return compact('user');
	}
}

?>