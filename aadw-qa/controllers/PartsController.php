<?php

namespace app\controllers;

use app\models\Parts;
use lithium\action\DispatchException;

class PartsController extends \lithium\action\Controller {

	public function index() {
		$parts = Parts::all();
		return compact('parts');
	}

	public function view() {
		$part = Parts::first($this->request->id);
		return compact('part');
	}

	public function add() {
		$part = Parts::create();

		if (($this->request->data) && $part->save($this->request->data)) {
			return $this->redirect(array('Parts::view', 'args' => array($part->id)));
		}
		return compact('part');
	}

	public function edit() {
		$part = Parts::find($this->request->id);

		if (!$part) {
			return $this->redirect('Parts::index');
		}
		if (($this->request->data) && $part->save($this->request->data)) {
			return $this->redirect(array('Parts::view', 'args' => array($part->id)));
		}
		return compact('part');
	}

	public function delete() {
		if (!$this->request->is('post') && !$this->request->is('delete')) {
			$msg = "Parts::delete can only be called with http:post or http:delete.";
			throw new DispatchException($msg);
		}
		Parts::find($this->request->id)->delete();
		return $this->redirect('Parts::index');
	}
}

?>