<?php

namespace app\controllers;

use app\models\Pics;
use lithium\action\DispatchException;

class PicsController extends \lithium\action\Controller {
    
    public $publicActions = array('index','view');

	public function index() {
		$pics = Pics::all();
		return compact('pics');
	}

	public function view() {
		$pic = Pics::first($this->request->id);
		return compact('pic');
	}

	public function add() {
		$pic = Pics::create();

		if (($this->request->data) && $pic->save($this->request->data)) {
			return $this->redirect("/q/edit/".$pic->post_slug);
		}
        
		return compact('pic');
	}

	public function edit() {
		$pic = Pics::find($this->request->id);

		if (!$pic) {
			return $this->redirect('Pics::index');
		}
		if (($this->request->data) && $pic->save($this->request->data)) {
			return $this->redirect(array('Pics::view', 'args' => array($pic->id)));
		}
		return compact('pic');
	}

	public function delete() {
		if (!$this->request->is('post') && !$this->request->is('delete')) {
			$msg = "Pics::delete can only be called with http:post or http:delete.";
			throw new DispatchException($msg);
		}
		Pics::find($this->request->id)->delete();
		return $this->redirect('Pics::index');
	}
}

?>