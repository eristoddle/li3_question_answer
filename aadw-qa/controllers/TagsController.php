<?php

namespace app\controllers;

use app\models\Tags;
use lithium\action\DispatchException;

class TagsController extends \lithium\action\Controller {
    
    public $publicActions = array('index','view');

	public function index() {
		$tags = Tags::all();      
		return compact('tags');
	}

	public function view() {
		//$tag = Tags::first($this->request->id);
        $tag = Tags::find('first', array(
				'conditions'=> array(
                    'tag' => $this->request->tag
                )
			)
		);
        
		return compact('tag');
	}

	public function add() {
		$tag = Tags::create();

		if (($this->request->data) && $tag->save($this->request->data)) {
			return $this->redirect(array('Tags::view', 'args' => array($tag->id)));
		}
		return compact('tag');
	}

	public function edit() {
		$tag = Tags::find($this->request->id);

		if (!$tag) {
			return $this->redirect('Tags::index');
		}
		if (($this->request->data) && $tag->save($this->request->data)) {
			return $this->redirect(array('Tags::view', 'args' => array($tag->id)));
		}
		return compact('tag');
	}

	public function delete() {
		if (!$this->request->is('post') && !$this->request->is('delete')) {
			$msg = "Tags::delete can only be called with http:post or http:delete.";
			throw new DispatchException($msg);
		}
		Tags::find($this->request->id)->delete();
		return $this->redirect('Tags::index');
	}
}

?>