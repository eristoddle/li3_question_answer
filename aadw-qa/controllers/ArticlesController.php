<?php

namespace app\controllers;

use app\models\Articles;
use app\models\Users;
use lithium\storage\Session;
use lithium\action\DispatchException;
use li3_flash_message\extensions\storage\FlashMessage;

class ArticlesController extends \lithium\action\Controller {
    
    public $publicActions = array('index','view');

	public function index() {
		$limit = 10;
        $page = $this->request->page ?: 1;
        $order = array('created' => 'DESC');
        $total = Articles::count();
        $articles = Articles::all(compact('order','limit','page'));
        return compact('articles', 'total', 'page', 'limit');
	}

	public function view() {
        $article = Articles::find('first', array(
				'conditions'=> array(
                    'slug' => $this->request->slug
                )
			)
		);
        
        $user = Users::find(Session::read('user._id'));
        
		return compact('article', 'user');
	}

	public function add() {
        $user = Users::find(Session::read('user._id'));
        
        if($user->role == 'admin') {
            $article = Articles::create();

            if (($this->request->data) && $article->save($this->request->data)) {
                return $this->redirect(array('Articles::view', 'slug' => $article->slug));
            }
            return compact('article');
        }
        
        FlashMessage::write('Only admin users can access this page.');
        return $this->redirect('Articles::index');
	}

	public function edit() {
        $user = Users::find(Session::read('user._id'));
        
        if($user->role == 'admin') {
            $article = Articles::find('first', array(
                    'conditions'=> array(
                        'slug' => $this->request->slug
                    )
                )
            );

            if (!$article) {
                return $this->redirect('Articles::index');
            }

            if (($this->request->data) && $article->save($this->request->data)) {
                return $this->redirect(array('Articles::view', 'slug' => $article->slug));
            }

            return compact('article');
        }
        
        FlashMessage::write('Only admin users can access this page.');
        return $this->redirect('Articles::index');
	}

	public function delete() {
		if (!$this->request->is('post') && !$this->request->is('delete')) {
			$msg = "Articles::delete can only be called with http:post or http:delete.";
			throw new DispatchException($msg);
		}
		Articles::find($this->request->id)->delete();
		return $this->redirect('Articles::index');
	}
}

?>