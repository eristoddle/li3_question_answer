<?php

use lithium\action\Dispatcher;
use lithium\action\Response;
use lithium\security\Auth;
use lithium\security\Password;
use lithium\util\collection\Filters;
use lithium\analysis\Logger;
use lithium\data\Connections;
use lithium\storage\Session;
use li3_access\security\Access;

use li3_flash_message\extensions\storage\FlashMessage;

/**
 * improved authentication using filters
 * http://lithify.me/docs/manual/lithium-basics/filters.wiki
 */
Dispatcher::applyFilter('_callable', function($self, $params, $chain) {
    
    $ctrl = $chain->next($self, $params, $chain);
    
    //If public action, don't check
    if (isset($ctrl->publicActions) && in_array($params['request']->action, $ctrl->publicActions)) {
        return $ctrl;
    }
    
    //Check User
    if ($user = Auth::check('user')){
        return $ctrl;
    }
	
    //If nothing makes it through, login
    //TODO: Change location to below to non-hard url. Currently Users::login not working. Goes to root
    return function() {
		FlashMessage::write('Only logged-in users can access this page.');
        return new Response(compact('request') + array('location' => '/aadw-qa/login'));
        //return new Response(compact('request') + array('location' => 'Users::login'));
    };
	
});

?>