<?php
use lithium\security\Auth;

use li3_access\security\Access;

use app\models\Users;

$accountsEmpty = Users::count();

//Rbac
Access::config(array(
    'rbac' => array(
        'adapter' => 'AuthRbac',
        'roles' => array(
            array(
                'match' => array('Questions::add'),
                'requesters' => array('default'),
                'message' => 'Log in to add posts.',
                'redirect' => 'Users::login',
            ),
            array(
                'match' => array('*::*'),
                'requesters' => array('admin'),
                'message' => 'Log in to add users.',
                'redirect' => 'Users::login',
            ),
            array(
                'match' => array('*::*'),
                'requesters' => array('contributor'),
                'message' => 'Log in to add users.',
                'redirect' => 'Users::login',
            ),
            array(
                'match' => array('*::*'),
                'requesters' => array('editor'),
                'message' => 'Log in to add users.',
                'redirect' => 'Users::login',
            ),
        )
    )
));

?>
