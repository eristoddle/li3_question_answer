<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2011, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

/**
 * The routes file is where you define your URL structure, which is an important part of the
 * [information architecture](http://en.wikipedia.org/wiki/Information_architecture) of your
 * application. Here, you can use _routes_ to match up URL pattern strings to a set of parameters,
 * usually including a controller and action to dispatch matching requests to. For more information,
 * see the `Router` and `Route` classes.
 *
 * @see lithium\net\http\Router
 * @see lithium\net\http\Route
 */
use lithium\net\http\Router;
use lithium\core\Environment;
use \lithium\storage\Session;

/**
 * Here, we are connecting `'/'` (the base path) to controller called `'Pages'`,
 * its action called `view()`, and we pass a param to select the view file
 * to use (in this case, `/views/pages/home.html.php`; see `app\controllers\PagesController`
 * for details).
 *
 * @see app\controllers\PagesController
 */

/**
 *Index Page 
 */
Router::connect('/', 'Posts::index');
//Pagination
Router::connect('/page/{:page:[0-9]+}', 'Posts::index');
Router::connect('/page/{:page:[0-9]+}/{:args}', 'Posts::index');

/**
 *Users 
 */
Router::connect('/login', 'Users::login');
Router::connect('/logout', 'Users::logout');
Router::connect('/register', 'Users::add');
Router::connect('/dashboard', 'Users::dashboard');

if(Session::read('user')){
	Router::connect('/settings', 'Users::edit');
	if (Session::read('user.role') == 'admin'){     
		Router::connect('/admin/{:controller}/{:action}/page:{:page:[0-9]+}', array('admin' => true), array('persist' => array('controller')));
		Router::connect('/admin/{:controller}/{:action}/{:id:[0-9a-f]{24}}.{:type}', array('id' => null, 'admin' => true), array('persist' => array('controller')));
		Router::connect('/admin/{:controller}/{:action}/{:id:[0-9a-f]{24}}', array('admin' => true), array('persist' => array('controller')));
		Router::connect('/admin/{:controller}/{:action}/{:args}', array('admin' => true), array('persist' => array('controller')));
	}
}

/**
 * BootstrapPaginator helper
 * TODO: Do per specific route
 */
//Router::connect('/{:controller}/{:action}/page/{:page:[0-9]+}');
//Router::connect('/{:controller}/{:action}/page/{:page:[0-9]+}/{:args}');

/**
 * Post slugs
 */
Router::connect('/q/add',array('Posts::add'));
Router::connect('/q/prefill',array('Posts::prefill'));
//Don't know why I need two
Router::connect('/q/{:slug:[A-Za-z0-9-]+}',array('Posts::view'));
Router::connect('/q/{:slug:[A-Za-z0-9-]+}', array('controller' => 'Posts', 'action' => 'View'));
Router::connect('/q/edit/{:slug:[A-Za-z0-9-]+}',array('Posts::edit'));

/**
 * Article slugs
 */
Router::connect('/articles/',array('Articles::index'));
//Pagination
Router::connect('/articles/page/{:page:[0-9]+}', 'Articles::index');
Router::connect('/articles/page/{:page:[0-9]+}/{:args}', 'Articles::index');
Router::connect('/articles/add',array('Articles::add'));
Router::connect('/a/{:slug:[A-Za-z0-9-]+}',array('Articles::view'));
Router::connect('/a/{:slug:[A-Za-z0-9-]+}', array('controller' => 'Articles', 'action' => 'View'));
Router::connect('/a/edit/{:slug:[A-Za-z0-9-]+}',array('Articles::edit'));

/**
 * Video slugs
 */
Router::connect('/videos/',array('Videos::index'));
//Pagination
Router::connect('/videos/page/{:page:[0-9]+}', 'Videos::index');
Router::connect('/videos/page/{:page:[0-9]+}/{:args}', 'Videos::index');
Router::connect('/videos/add',array('Videos::add'));
Router::connect('/v/{:slug:[A-Za-z0-9-]+}',array('Videos::view'));
Router::connect('/v/{:slug:[A-Za-z0-9-]+}', array('controller' => 'Videos', 'action' => 'View'));
Router::connect('/v/edit/{:slug:[A-Za-z0-9-]+}',array('Videos::edit'));

/**
 * Tag slugs
 */
Router::connect('/tags/',array('Tags::index'));;
Router::connect('/tags/{:tag:[A-Za-z0-9-]+}',array('Tags::view'));

/**
 * Connect the rest of `PagesController`'s URLs. This will route URLs like `/pages/about` to
 * `PagesController`, rendering `/views/pages/about.html.php` as a static page.
 */
//Router::connect('/pages/{:args}', 'Pages::view');

/**
 * Add the testing routes. These routes are only connected in non-production environments, and allow
 * browser-based access to the test suite for running unit and integration tests for the Lithium
 * core, as well as your own application and any other loaded plugins or frameworks. Browse to
 * [http://path/to/app/test](/test) to run tests.
 */
if (!Environment::is('production')) {
	Router::connect('/test/{:args}', array('controller' => 'lithium\test\Controller'));
	Router::connect('/test', array('controller' => 'lithium\test\Controller'));
}

/**
 * ### Database object routes
 *
 * The routes below are used primarily for accessing database objects, where `{:id}` corresponds to
 * the primary key of the database object, and can be accessed in the controller as
 * `$this->request->id`.
 *
 * If you're using a relational database, such as MySQL, SQLite or Postgres, where the primary key
 * is an integer, uncomment the routes below to enable URLs like `/posts/edit/1138`,
 * `/posts/view/1138.json`, etc.
 */
// Router::connect('/{:controller}/{:action}/{:id:\d+}.{:type}', array('id' => null));
// Router::connect('/{:controller}/{:action}/{:id:\d+}');

/**
 * If you're using a document-oriented database, such as CouchDB or MongoDB, or another type of
 * database which uses 24-character hexidecimal values as primary keys, uncomment the routes below.
 */
 Router::connect('/{:controller}/{:action}/{:id:[0-9a-f]{24}}.{:type}', array('id' => null));
 Router::connect('/{:controller}/{:action}/{:id:[0-9a-f]{24}}');

/**
 * Finally, connect the default route. This route acts as a catch-all, intercepting requests in the
 * following forms:
 *
 * - `/foo/bar`: Routes to `FooController::bar()` with no parameters passed.
 * - `/foo/bar/param1/param2`: Routes to `FooController::bar('param1, 'param2')`.
 * - `/foo`: Routes to `FooController::index()`, since `'index'` is assumed to be the action if none
 *   is otherwise specified.
 *
 * In almost all cases, custom routes should be added above this one, since route-matching works in
 * a top-down fashion.
 */
Router::connect('/{:controller}/{:action}/{:args}');

?>