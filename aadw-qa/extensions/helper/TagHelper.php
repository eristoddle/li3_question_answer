<?php

 /**
Description: Lithium Helper for Tags on Sidebar
*/

namespace app\extensions\helper;

class TagHelper extends \lithium\template\Helper {
	
	public function tagsToString($tags) {		
		$html = "";
		$count = 0;
		
        if (count($tags) > 0 && is_object($tags)){
            foreach( $tags as $tag ){
                if( $count==0 ){
                    $count++;
                    $html = $tag;
                    continue;
                }			
                $html .= "," .$tag;
            }
        }
		
		return $html;		
	}
    
    public function tagsToInlineLabels($tags) {		
		$html = "";
		
        if (count($tags) > 0 && is_object($tags)){
            foreach( $tags as $tag ){	
                $html .= '<span class="label label-info"><a href="/aadw-qa/tags/'.$tag.'">'.$tag.'</a></span> ';
            }
        }
		
		return $html;	
	}
	
}