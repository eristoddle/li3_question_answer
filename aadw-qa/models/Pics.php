<?php

namespace app\models;

class Pics extends \lithium\data\Model {

	public $_schema = array(
		'_id' => array('type'=>'id'),
        //'user_id' => array('type'=>'string','null'=>false),
        //'public' => array('type' => 'boolean','null'=>false),
		'post_id' => array('type'=>'_id','null'=>false),
        'filename' => array('type'=>'string','null'=>false),
		'uploadDate' => array('type'=>'date','null'=>false),
        'length' => array('type'=>'int','null'=>false),
		'chunkSize' => array('type'=>'int','null'=>false),
        'md5' => array('type'=>'string','null'=>false),
	);

	public $validates = array(
    );
    
    protected $_meta = array('source' => 'fs.files');
}

?>