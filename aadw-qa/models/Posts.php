<?php

namespace app\models;

class Posts extends \app\models\BaseModel {

	public $_schema = array(
		'_id' => array('type'=>'id'),
        //'user_id' => array('type'=>'string','null'=>false),
        //'public' => array('type' => 'boolean','null'=>false),
		'title' => array('type'=>'string','null'=>false),
        'description' => array('type'=>'string','null'=>false),
		'slug' => array('type'=>'string','null'=>false),
        'tags' => array('type'=>'array'),
		'modified' => array('type'=>'date','null'=>false),
	);

	public $validates = array(
        'title' => array(
			array('lengthBetween', 'min' => 1, 'max' => 80,
				'message' => 'Please enter an article title between 1 and 80 characters')
		),
        'description' => array(
            array('notEmpty', 'message' => 'Description body is empty'),
        ),
    );
    
    protected $_user = null;
    
    public function user($record) {
        if (!empty($record->_user)) {
                return $record->_user;
        }
        $_user = Users::find($record->user_id);
        return $record->_user = $_user;
    }
   
}

//TODO: Add Moderation value
//TODO: Add User Id
    
//Filters::apply('app\models\Posts', 'save', function($self, $params, $chain) {
//    
//    //TODO: Some filters apply to only questions or answers
//
//    if ($params['data']) {
//        $params['entity']->set($params['data']);
//        $params['data'] = array();
//    }
//    
//    return $chain->next($self, $params, $chain);
//
//});

?>