<?php

namespace app\models;

//use lithium\util\collection\Filters;

class Tags extends \lithium\data\Model {

	public $validates = array();
    
    protected $_schema = array(
        '_id' => array('type' => 'id'),
        'content' => array('type' => 'array', 'array' => true)
    );
     
}

//Filters::apply('app\models\Tags', 'save', function($self, $params, $chain) {
//
//    if ($params['data']) {
//        $params['entity']->set($params['data']);
//        $params['data'] = array();
//    }
//
//    return $chain->next($self, $params, $chain);
//
//});

?>