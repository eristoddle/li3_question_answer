<?php

namespace app\models;

use lithium\util\collection\Filters;
use lithium\util\Inflector;
use lithium\security\Auth;

use MongoDate;

class BaseModel extends \lithium\data\Model {
    
    public static function __init() {
        parent::__init();
 
        // {{{ Filters
        static::applyFilter('save', function($self, $params, $chain) {
            
            if ($params['data']) {
                $params['entity']->set($params['data']);
                $params['data'] = array();
            }
 
            //do these things only if they don't exist (i.e.  on creation of object)
            if (!$params['entity']->exists()) {
                
                /* Slug */	
                $slug = Inflector::slug($params['entity']->title);
                
                $count = $self::find('count', array(
                    'fields' => array('id'),
                    'conditions' => array('slug' => array('like' => '/^(?:' . $slug . ')(?:-?)(?:\d?)$/i')),
                ));
                $params['data']['slug'] = $slug . ($count ? "-" . (++$count) : '');

                /* User */
                $user = Auth::check('user');
            	$params['data']['user_id'] = $user['_id'];
                
            }
            
            /* Tags */
            if (!empty($params['entity']->tags)) {
                $t_array = explode(",",$params['entity']->tags);
                foreach($t_array as $t){
                    $f_tag = strtolower(trim($t));
                    $f_array[] = $f_tag;
                    if ($oldtag = Tags::find('first', array(
                        'conditions' => array(
                            'tag' => $f_tag
                        )
                    ))) {
                        $found = false;
                        foreach($oldtag->content as $c){
                            if ($c['_id'] == $params['entity']->_id) {
                                $found = true;
                                break;
                            } 
                        }
                        if(!$found) {
                            $query = array(
                                '$push'=> array(
                                    'content'=>array(
                                            'model' => $self,
                                            'title' => $params['entity']->title,
                                            'slug' => $params['entity']->slug,
                                            '_id' => $params['entity']->_id
                                        )
                                    )
                            );
                            $conditions = array('_id'=>$oldtag->_id);
                            Tags::update($query, $conditions, array('atomic' => false));             
                        }
                    }else{
                        $tag = Tags::create();
                        $tag->tag = $f_tag;
                        $tag->content[] = array(
                            'model' => $self,
                            'title' => $params['entity']->title,
                            'slug' => $params['entity']->slug,
                            '_id' => $params['entity']->_id
                        );
                        $tag->save();
                    }
                }
                //NOTE: Array must be object here
                $params['entity']->tags = (object)$f_array;
            }

            /* Timestamp */
            $params['entity']->modified = new MongoDate();
 
            return $chain->next($self, $params, $chain);
        });
        // }}}
    }
    
    /* Interesting method may be useful */
    protected function mapReduce($map, $reduce, $out = null){
    	$options = array(
		    "mapreduce" => static::meta('source'), 
		    "map" => $map,
		    "reduce" => $reduce,
		);

		if($out){
			$options['out'] = $out;
		}

    	return static::connection()->connection->command($options);
    }
    
}

?>