<?php

namespace app\models;

use lithium\util\collection\Filters;

class Videos extends \app\models\BaseModel {

	public $_schema = array(
		'_id' => array('type'=>'id'),
        //'user_id' => array('type'=>'string','null'=>false),
        //'public' => array('type' => 'boolean','null'=>false),
		'title' => array('type'=>'string','null'=>false),
        'youtube_embed_url' => array('type'=>'string','null'=>false),
		'youtube_url' => array('type'=>'string','null'=>false),
		'slug' => array('type'=>'string','null'=>false),
        'tags' => array('type'=>'array'),
		'modified' => array('type'=>'date','null'=>false),
	);

	public $validates = array(
        'title' => array(
			array('lengthBetween', 'min' => 1, 'max' => 80,
				'message' => 'Please enter an article title between 1 and 80 characters')
		),
        'body' => array(
            array('notEmpty', 'message' => 'Description body is empty'),
        ),
        'youtube_url' => array(
            array('notEmpty', 'message' => 'YouTube url is empty'),
        ),
    );
}

Filters::apply('app\models\Videos', 'save', function($self, $params, $chain) {
    
    if ($params['data']) {
        $params['entity']->set($params['data']);
        $params['data'] = array();
    }
 
    /* Video Embed Url */
    $params['data']['youtube_embed_url'] = str_replace('youtu.be/','www.youtube.com/embed/',$params['entity']->youtube_url);

   return $chain->next($self, $params, $chain);
});

?>