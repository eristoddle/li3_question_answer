<?php

namespace app\models;

class Articles extends \app\models\BaseModel {
    
    public $_schema = array(
		'_id' => array('type'=>'id'),
        //'user_id' => array('type'=>'string','null'=>false),
        //'public' => array('type' => 'boolean','null'=>false),
		'title' => array('type'=>'string','null'=>false),
		'body' => array('type'=>'string','null'=>false),
		'slug' => array('type'=>'string','null'=>false),
        'tags' => array('type'=>'array'),
		'modified' => array('type'=>'date','null'=>false),
	);

	public $validates = array(
        'title' => array(
			array('lengthBetween', 'min' => 1, 'max' => 80,
				'message' => 'Please enter an article title between 1 and 80 characters')
		),
        'body' => array(
            array('notEmpty', 'message' => 'Article body is empty'),
        )
    );
}

//Filters::apply('app\models\Articles', 'save', function($self, $params, $chain) {
//
//    if ($params['data']) {
//        $params['entity']->set($params['data']);
//        $params['data'] = array();
//    }
//	
//    return $chain->next($self, $params, $chain);
//
//});

?>