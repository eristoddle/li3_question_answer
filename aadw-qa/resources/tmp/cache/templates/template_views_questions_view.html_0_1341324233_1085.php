<article>
    <h1>
        <?php echo $h($post->title); ?>
    </h1>
    <p><?php echo $h($post->description); ?></p>
</article>

<?php if (count($answers)): ?>
<hr />
<h2>Answers</h2>
    <? foreach($answers as $answer): ?>
        <div class="well">
            <h3>
                <?php echo $h($answer->title); ?>
            </h3>
            <p><?php echo $h($answer->description); ?></p>
        </div>
    <? endforeach; ?>
<hr />
<?php endif; ?>

<?php echo $this->form->create(null,array(
            'action' => 'add'
)); ?>
    <?php echo $this->form->field('title',array(
            'label' => 'Answer'
        )); ?>
    <?php echo $this->form->field('description',array(
        'type' => 'textarea',
        'cols' => '100',
        'rows' => '10'
    )); ?>
    <?php echo $this->form->field('product_link'); ?>
    <?php echo $this->form->field('post_type',array(
        'type' => 'hidden',
        'value' => 'answer'
    )); ?>
    <?php echo $this->form->field('answer_id',array(
        'type' => 'hidden',
        'value' => $post->_id
    )); ?>
    <?php echo $this->form->submit('Answer'); ?>
<?php echo $this->form->end(); ?>