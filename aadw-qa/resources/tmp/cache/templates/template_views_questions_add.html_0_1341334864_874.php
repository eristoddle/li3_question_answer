<?php if(!isset($post)) {$post = null; }?>
<?php if(!isset($title)) {$title = null; }?>
<?php echo $this->form->create($post, array(
            'action' => 'add'
        )); ?>
    <? if($title): ?>
        <?php echo $this->form->field('title',array(
            'value' => $title
        )); ?>
    <? endif; ?>
    <? if(!$title): ?>
        <?php echo $this->form->field('title'); ?>
    <? endif; ?>
    <?php echo $this->form->field('description',array(
        'type' => 'textarea',
        'cols' => '100',
        'rows' => '10'
    )); ?>
    <?php echo $this->form->field('product_link'); ?>
    <?php echo $this->form->field('post_type',array(
        'type' => 'hidden',
        'value' => 'question'
    )); ?>
    <?php echo $this->form->submit('Save'); ?>
<?php echo $this->form->end(); ?>
<?php
	/*Data Dump*/
//	echo '<pre>';
//	print_r($post);
//    print_r($title);
//	echo '</pre>';
?>