<?php
if(!isset($post)){ $post = null; }
?>
<?php echo $this->form->create($post, array(
				'action' => 'prefill'
			)); ?>
		<?php echo $this->form->field('title', array(
				'label' => 'Question'
			)); ?>
		<?php echo $this->html->link('Advanced Question Form', 'Questions::add'); ?>.
		<?php echo $this->form->submit('Ask'); ?>
	<?php echo $this->form->end(); ?>
<?php if(!empty($posts)): ?>
	<?php if (count($posts)): ?>
		<? foreach($posts as $post): ?>
			<article>
				<h2>
					<? //TODO: No hard paths ?>
					<?php echo $this->html->link($post->title,'/questions/view/'.$post->_id); ?>
					</a>
				</h2>
				<p><?php echo $h($post->description); ?></p>
			</article>
		<hr/>
		<? endforeach; ?>
	<?php endif; ?>
<?php endif; ?>