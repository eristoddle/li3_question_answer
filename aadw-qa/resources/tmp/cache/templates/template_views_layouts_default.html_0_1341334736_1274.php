<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2011, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */
?>
<!doctype html>
<html>
<head>
	<?php echo $this->html->charset();?>
	<title>Application &gt; <?php echo $this->title(); ?></title>
	<?php echo $this->html->style(array('debug', 'lithium')); ?>
    <?php echo $this->html->style(array('bootstrap','bootstrap-responsive','app')); ?>
	<?php echo $this->scripts(); ?>
    <?php echo $this->html->script('head.js'); ?>
	<?php echo $this->html->link('Icon', null, array('type' => 'icon')); ?>
</head>
<body class="app">
	<div id="container">
		<div id="header">
			<h1><?php echo $this->html->link('DoorAndWindowDIY', 'Questions::index'); ?></h1>
			<h2>
				Powered by <?php echo $this->html->link('Lithium', 'http://lithify.me/'); ?>.
			</h2>
		</div>
		<div id="content">
			<?php echo $this->content(); ?>
		</div>
	</div>
    <script type="text/javascript" charset="utf-8">
		head.js(
			"<?php echo $this->url('/js/jquery.min.js'); ?>",
			"<?php echo $this->url('/js/icanhaz.min.js'); ?>",
			"<?php echo $this->url('/js/bootstrap.min.js'); ?>",
			"<?//=$this->url('/js/app.js');?>",
			function() {
				ich.grabTemplates();
			}
		);
	</script>
</body>
</html>